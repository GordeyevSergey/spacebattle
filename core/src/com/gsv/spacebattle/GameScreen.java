package com.gsv.spacebattle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

/**
 * Класс процесса игры
 */
public class GameScreen implements Screen {
    private final Battle game;
    private final int OBJ_SIZE = 64; //Размер объектов
    private int score = 0; //Игровой счет
    private int killsCount = 0;
    private OrthographicCamera camera;

    private long lastEnemySpawn;
    private long lastBulletSpawn;

    private Ship spaceship;

    private Array<Bullets> bulletList;
    private Array<Ship> enemyList;

    private Texture gamebg;

    GameScreen(final Battle game) {
        this.game = game;
        gamebg = new Texture("gamebg.png");
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Battle.WIDTH, Battle.HEIGHT);
        spaceship = new Ship(Battle.WIDTH/2F - 64/2F, 20, 0, OBJ_SIZE, OBJ_SIZE, "ship80px.png");

        enemyList = new Array<Ship>();
        spawnEnemy();

        bulletList = new Array<Bullets>();
        spawnBullet();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();

        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(gamebg, 0, 0, Battle.WIDTH, Battle.HEIGHT);
        game.font.draw(game.batch, "Kills:" + killsCount, Battle.WIDTH/2F, Battle.HEIGHT - 10); //Строка счетчика сбитых противников
        game.font.draw(game.batch, "Score: " + score, 5, Battle.HEIGHT - 10); //Строка текущего ранга игрока
        game.batch.draw(spaceship.getTexture(), spaceship.x, spaceship.y);

        //Отрисовка противников
        for (Ship enemy : enemyList) {
            game.batch.draw(enemy.getTexture(), enemy.x, enemy.y);
        }

        //Отрисовка снарядов
        for (Bullets bullet : bulletList) {
            game.batch.draw(bullet.getTexture(), bullet.x, bullet.y);
        }
        game.batch.end();

        spaceship.control();
        enemyStatus();
        bulletStatus();
    }

    private void enemyStatus() {
        //Генерация противника через промежуток времени spawnEnemyTimer
        int spawnEnemyTimer = MathUtils.random(800000000, 2000000000); // время до спавна противника
        if (TimeUtils.nanoTime() - lastEnemySpawn > spawnEnemyTimer) spawnEnemy();

        Iterator<Ship> enemyIterator = enemyList.iterator();
        while (enemyIterator.hasNext()) {
            Ship enemy = enemyIterator.next();
            //Движение противника вниз по экрану
            enemy.update();
            //Удаление противника из массива при выходе за пределы экрана
            if (enemy.y + OBJ_SIZE < 0) enemyIterator.remove();
            //Обработка попаданий снаряда по противнику
            for (Bullets bullet : bulletList) {
                if (bullet.overlaps(enemy)) {
                    enemyIterator.remove();
                    score+=10;
                    killsCount++;
                }
            }
            //Обработка столкновения корабля с противником
            if (spaceship.overlaps(enemy)) {
                game.setScreen(new MainMenuScreen(game));
            }
        }
    }

    private void bulletStatus() {
        //Генерация снаряда через промежуток времени spawnBulletTimer
        int spawnBulletTimer = 300000000; // время до спавна снаряда
        if (TimeUtils.nanoTime() - lastBulletSpawn > spawnBulletTimer) spawnBullet();

        Iterator<Bullets> bulletIterator = bulletList.iterator();
        while (bulletIterator.hasNext()) {
            Bullets bullet = bulletIterator.next();
            bullet.update();
            //Удаление снаряда из массива при выходе за пределы экрана
            if (bullet.y >= Battle.HEIGHT) bulletIterator.remove();
        }
    }

    /**
     * Метод генерации противников
     */
    private void spawnEnemy() {
        int randomX = MathUtils.random(0, Battle.WIDTH - OBJ_SIZE);
        int randomSpeed = MathUtils.random(200, 400);
        Ship enemy = new Ship(randomX, Battle.HEIGHT, randomSpeed, OBJ_SIZE, OBJ_SIZE, "enemy80px.png");
        enemyList.add(enemy);
        lastEnemySpawn = TimeUtils.nanoTime();
    }

    /**
     * Метод генерации снарядов
     */
    private void spawnBullet() {
        Bullets bullet = new Bullets(spaceship.x + spaceship.width / 2 - 5, spaceship.y + spaceship.height, 800, 20, 20, "bullet30px.png");
        bulletList.add(bullet);
        lastBulletSpawn = TimeUtils.nanoTime();
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        gamebg.dispose();
    }
}
