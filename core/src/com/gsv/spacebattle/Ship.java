package com.gsv.spacebattle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Класс модели корабля игрока
 */
class Ship extends Model {

    Ship(float x, float y, int speed, float width, float height, String img) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.height = height;
        this.width = width;
        this.texture = new Texture(img);
    }


    /**
     * Метод просчета выхода модели корабля игрока за пределы экрана
     */
    @Override
    void control() {
        if (Gdx.input.isTouched()) {
            x = Gdx.input.getX() - width;
            y = Battle.HEIGHT - Gdx.input.getY() + height;
            if (x > Battle.WIDTH - width) x = Battle.WIDTH - width;
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            if (y > Battle.HEIGHT) y = Battle.HEIGHT;
        }
    }

    /**
     * Метод перемещения корабля игрока по экрану
     */
    @Override
    void update() {
        y -= speed * Gdx.graphics.getDeltaTime();
    }

    /**
     * Метод просчета столкновения моделей
     * @param r Сравниваемая модель
     * @return результат просчета
     */
    @Override
    public boolean overlaps(Model r) {
        return super.overlaps(r);
    }

}
