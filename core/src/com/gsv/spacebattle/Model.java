package com.gsv.spacebattle;

import com.badlogic.gdx.graphics.Texture;

/**
 * Базовый класс модели, содержащий в себе все необходимые пармаетры
 */
class Model {
    float x;
    float y;
    float width;
    float height;
    int speed;
    Texture texture;

    void update() {
    }

    void control() {
    }

    /**
     * Класс просчета столкновения моделей
     * @param r Сравниваемая модель
     * @return Результат проверки столкновения
     */
    public boolean overlaps(Model r) {
        return x < r.x + r.width && x + width > r.x && y < r.y + r.height && y + height > r.y;
    }

     Texture getTexture() {
        return texture;
    }

}
