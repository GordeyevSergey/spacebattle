package com.gsv.spacebattle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

/**
 * Класс главного меню игры
 */
public class MainMenuScreen implements Screen {
    private Texture background;
    private Texture playButton; //Кнопка "играть"
    private final Battle game;
    private OrthographicCamera camera;

    MainMenuScreen(Battle game) {
        this.game = game;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, Battle.WIDTH, Battle.HEIGHT);
        background = new Texture("bg.png");
        playButton = new Texture("playbutton.png");
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(background, 0, 0, Battle.WIDTH, Battle.HEIGHT);
        game.batch.draw(playButton, Battle.WIDTH / 2 - playButton.getWidth() / 2, Battle.HEIGHT / 4);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new GameScreen(game)); //Переход на экран игры
            dispose();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        background.dispose();
        playButton.dispose();
    }
}
