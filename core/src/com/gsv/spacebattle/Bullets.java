package com.gsv.spacebattle;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

/**
 * Класс модели снарядов
 */
class Bullets extends Model {

    Bullets(float x, float y, int speed, float width, float height, String img) {
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.height = height;
        this.width = width;
        this.texture = new Texture(img);
    }

    /**
     * Класс, просчитывающий соприкосновение моделей
     * @param r Сравниваемая модель
     * @return результат проверки на соприкосновение моделей
     */
    @Override
    public boolean overlaps(Model r) {
        return super.overlaps(r);
    }

    /**
     * Класс, отвечающий за перемещение снаряда по экрану
     */
    @Override
    void update() {
        y += speed * Gdx.graphics.getDeltaTime();
    }
}
